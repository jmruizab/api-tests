/*
// Documentación
  https://developers.facebook.com/docs/javascript/quickstart
// Obtener el token de acceso:
  https://developers.facebook.com/tools/explorer/145634995501895/
*/

import React, { Component } from 'react';
import _ from 'lodash';
import graph from 'fb-react-sdk';

const access_token = "<https://developers.facebook.com/tools/explorer/145634995501895/>";

class App extends Component {
  constructor(){
    super();
    graph.setAccessToken(access_token);
    this.test = "";
  }

  componentWillMount(){
    this.search();
  }

  search=()=>{
    graph.get("me?fields=friends{last_name,education,name,location,gender,about}", (err, res) => {
      console.log("response", err, res);
      this.test = res;
      this.forceUpdate();
    });

  }

  render() {
    return (
      <div className="App">
        <div>
          <p>{JSON.stringify(this.test, null, '')}</p>
        </div>
      </div>
    );
  }
}

export default App;
